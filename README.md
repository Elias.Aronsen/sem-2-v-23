# CONNECT 4 - Semester Oppgave 2 - Elias Ruud Aronsen
![connect 4](https://play-lh.googleusercontent.com/lZr9XlmL3TWpwqMDI_WF5k_ud9UeVW6QLzAnb17_bsyjfJBIGZxDLfLEN2AqCW2JD9A=w240-h480-rw)

__**Spill mot en venn! I Connect 4 skal du slippe en brikke med din farge hver tur. Første man til 4 brikker på rad eller skrå vinner!**__


### Hvordan spiller man?
- Beveg på piltastene eller pek musepekeren og klikk på kolonnen du har lyst til å slippe brikken og trykk ENTER/SPACE!
- Når du er ferdig med din tur gi kontrollen over til vennen din!
- Trykk på 'y' eller 'n' for musikk eller stillhet!

#### Når du er ferdig...
- Trykk 'q' for å lukke spillet...
- ... Eller trykk 'R' for å spille igjen!



### Litt mer info:
- Spillet er 1 mot 1. 
- Du vinner en runde ved å få 4 av samme farge på råd, kolonner eller skrå.
- Spillet teller antall ganger hver farge har vunnet.
- Spillet blir uavgjort vis brettet blir fullt uten en vinner.


### Video av spillet!
![Connect_4_-_Sem2_Elias_Aronsen.web](/uploads/19c72a392cfee71e9c9a0f4500635b44/Connect_4_-_Sem2_Elias_Aronsen.web.mp4)

- Alternativ link til video: [Youtube-link](https://www.youtube.com/watch?v=DrseC61armQ)

***Noe av koden brukt er fra UIB INF101 V23 Semester Oppgave 1.***

***Royalty free music er fra https://bitmidi.com/***
