package no.uib.inf101.sem2.view;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;


  /**
   * Takes in our box which contains our grid and using the given margin and GridDimension 
   * finds the coordinates and size (in pixels) of the each cell/tile and creates a Rectangle2D.Double-object
   * with the given values in pixels.
   * @param box Box which contains grid
   * @param gd The size of the grid, in the form of rows and cols.
   * @param margin The extra space between cells including margin between the <code>box</code>
   * and the outer cells.
   */
public class CellPositionToPixelConverter {
  
    private final Rectangle2D box;  // Box which contains grid
    private final GridDimension gd; // The size of the grid, by rows and cols
    private final double margin;    // The amount of space between cells including margin between the <code>box</code>
    private double cellWidth;
    private double cellHeight;

    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin){ 
      this.box = box;
      this.gd = gd;
      this.margin = margin;

      double PixelsForCellsWidth = box.getWidth() - (gd.cols() + 1)*margin;   // returns total amount of pixels for cellsWidth
      this.cellWidth = PixelsForCellsWidth/gd.cols();                              // our cellWidth

      double PixelsForCellsHeight = box.getHeight() - (gd.rows() + 1)*margin;  // returns total amount of pixels for cellsHeight
      this.cellHeight = PixelsForCellsHeight/gd.rows();                    // our cellHeight
    }
    

    
    /**
     * Uses the given values of the field-varaibles and CellPosition and calculates
     * the size and postion of the cell in pixels.
     * @param cp position of the cell in the form of rows and columns.
     * @return the Rectangle2D-object of right position and size.
     */
    public Ellipse2D getBoundsForCell(CellPosition cp){
      
      if(cp.row() == 0){
        return firstRowCells(cp);
      }else{
        //X
        double cellX = box.getX() + margin + cp.col()*(cellWidth + margin); //our x-coordinate for the cell
    
        //Y
        double cellY = box.getY() + margin + cp.row()*(cellHeight + margin); //our y-coordinate for the cell
    
        return new Ellipse2D.Double(cellX, cellY, cellWidth, cellHeight);
      }
    }
    


    /**
     * This to move the first "invisible " row slightly up to avoid it clipping into the board.
     * @param cp position of the cell in the form of rows and columns.
     * @return returns the circles for the first row, will be made invisible in view.
     */
    private Ellipse2D firstRowCells(CellPosition cp){
      // X
      double cellX = box.getX() + margin + cp.col()*(cellWidth + margin); //our x-coordinate for the cell
      //Y
      double cellY = box.getY()-20 + margin + cp.row()*(cellHeight + margin); //our y-coordinate for the cell
  
      return new Ellipse2D.Double(cellX, cellY, cellWidth, cellHeight);
    }
    



/**
   * Gets the CellPosition of the grid corresponding to the given (x, y)
   * coordinate. Returns null if the point does not correspond to a cell
   * in the grid.
   * SKREVET AV: TORSTEIN STRØMME
   * @param point a non-null pixel location
   * @return the corresponding CellPosition, or null if none exist
   */
  public CellPosition getCellPositionOfPoint(Point2D point) {
    ///
    // Same math as getBoundsForCell, but isolate the col/row on one side
    // and replace cellX with point.getX() (cellY with point.getY())
    double col = (point.getX() - box.getX() - margin) / (cellWidth + margin);
    double row = (point.getY() - box.getY() - margin) / (cellHeight + margin);

    // When row or col is out of bounds
    if (row < 0 || row >= gd.rows() || col < 0 || col >= gd.cols()) return null;

    // Verify that the point is indeed inside the bounds of the cell, and not on
    // the margin border
    CellPosition pos = new CellPosition((int) row, (int) col);
    if (getBoundsForCell(pos).contains(point)) {
      return pos;
    } else {
      return null;
    }
  }

}
