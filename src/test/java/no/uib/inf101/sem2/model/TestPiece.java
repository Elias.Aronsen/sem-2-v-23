package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;

public class TestPiece {


    @Test
    public void testHashCodeAndEquals() {
      Piece p1 = Piece.newPiece('r');
      Piece p2 = Piece.newPiece('r');
      Piece p3 = Piece.newPiece('r').shiftedBy(1, 0);
      Piece s1 = Piece.newPiece('b');
      Piece s2 = Piece.newPiece('b').shiftedBy(0, 0);
    
      assertEquals(p1, p2);
      assertEquals(s1, s2);
      assertEquals(p1.hashCode(), p2.hashCode());
      assertEquals(s1.hashCode(), s2.hashCode());
      assertNotEquals(p1, p3);
      assertNotEquals(p1, s1);
    }


    @Test
    public void testShiftedBy(){

        Piece p1 = Piece.newPiece('r');

        //cheks defalut position is (0,0)
        assertEquals(new CellPosition(0, 0),p1.getPos());

        p1 = p1.shiftedBy(2, 5);

        //cheks if shiftedby moved correctly
        assertEquals(new CellPosition(2, 5),p1.getPos());

        p1 = p1.shiftedBy(-20, -5);

        //cheks if shiftedby moved correctly 
        assertEquals(new CellPosition(-18, 0),p1.getPos());
    }
}
