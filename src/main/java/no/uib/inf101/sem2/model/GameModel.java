package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.controller.ControllableGameModel;
import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.view.ViewableGameModel;


/** This game model is where everything comes together, here the game is setup and manipulated after a set defined rules. */
public class GameModel implements ControllableGameModel, ViewableGameModel{

    private GameBoard board;
    private Piece piece;
    private GameState gameState = GameState.WELCOME_SCREEN;
    private Character currentColor = 'b';
    private CellPosition selectedPosition;
    private char currentWinner =  '-';
    private int redWins = 0;
    private int blueWins = 0;

    public GameModel(GameBoard board){
        this.board = board;
        this.selectedPosition = new CellPosition(0,board.cols()/2);
        switchColor();
        this.piece = Piece.newPiece(currentColor);
        setSelected(selectedPosition);
    }
    


    @Override
    public boolean movePiece(int deltaRow, int deltaCol) {
        Piece potentialShift = piece.shiftedBy(deltaRow, deltaCol);
        if(legalMove(potentialShift)){
            this.piece = potentialShift;
            return true;
        }
        return false;
    }


    @Override
    public void dropPiece() {
        //checks if there are available cells, stops if not
        if(!availableCells()){
            return;
        }
        //will drop the piece as far down it can legally
        while(movePiece(1, 0));
        //glues the piece into the board
        gluePieceToBoard();

        //checks for connect 4
        if(board.checkConnect4()){
            if(getColorCharacter() == 'r'){ //adds score to scoreboard
                currentWinner = 'r';
                redWins++;
            }
            else{
                currentWinner = 'b';
                blueWins++;
            }
            this.gameState = GameState.GAME_OVER; //makes it gameover
        }

        //checks for full board
        if(board.checkFullboard()){
            currentWinner = '-';
            this.gameState = GameState.GAME_OVER;
        }
        //sets new piece
        newPiece();
    }


    /**
     * Swithes the current color from red to blue and from blue to red.
     */
    private void switchColor(){
        if(this.currentColor == 'r'){
            this.currentColor = 'b';
        }else{
            this.currentColor = 'r';
        }
    }

    /** Checks if the move is placement of the given piece is legal */
    private boolean legalMove(Piece piece){
        if(!board.positionIsOnGrid(piece.getPos())){
            return false;
        } else if(this.board.get(piece.getPos()) != '-'){
            return false;
        } else{
            return true;
        }
    }

    /** Returns true or false depending on wheter there are available cells in this column */
    private boolean availableCells(){
        if(board.get(new CellPosition(1, selectedPosition.col())).equals('-')){
            return true;
        } else{
            return false;
        }
    }

    /**
     * Takes position of piece and updates the board to its values in its position.
     */
    private void gluePieceToBoard() {
        this.board.set(piece.getPos(), this.currentColor);
    }

    /**
     * Updates the current Piece to a new piece at the top position.
     * Also checks if its legal to create a new piece
     */
    private void newPiece() {
        switchColor();
        Piece newPiece = Piece.newPiece(currentColor);

        if(legalMove(newPiece)){
            this.piece = newPiece;
            setSelected(new CellPosition(0,board.cols()/2)); //moves selected column and new piece to center
        }
        
    }

    //** Corrects the pieces position to the selected column */
    private void pieceCorrector(){
        int colA = selectedPosition.col();
        int colB = this.piece.getPos().col();
        movePiece(0, colA-colB);
    }


    


    @Override
    public void setSelected(CellPosition selectedPosition) {
        this.selectedPosition = selectedPosition;

        //This part moves the piece to the selected position using the move function.
        pieceCorrector();
    }



    @Override
    public boolean moveSelected(int deltaCol){
        int row = this.selectedPosition.row();
        int col = this.selectedPosition.col();
        CellPosition newPos = new CellPosition(row, col + deltaCol);
        if(board.positionIsOnGrid(newPos)){
            this.selectedPosition = newPos;
            pieceCorrector();
            return true;
        } else{
            return false;
        }
    }



    @Override
    public int getRedWins() {
        return redWins;
    }


    @Override
    public int getBlueWins() {
        return blueWins;
    }


    @Override
    public char getWinner() {
        return currentWinner;
    }

    @Override
    public CellPosition getSelected() {
        return this.selectedPosition;
    }


    @Override
    public GameState getGameState() {
        return gameState;
    }


    @Override
    public Character getColorCharacter() {
        return currentColor;
    }

    @Override
    public GridDimension getDimension() {
        return board;
    }

    @Override
    public Iterable<GridCell<Character>> getTilesOnBoard() {
        return board;
    }

    @Override
    public Piece getPiece(){
        return this.piece;
    }


    @Override
    public void startGame() {
        this.gameState = GameState.ACTIVE_GAME;
    }

    @Override
    public void reset() {
        this.gameState = GameState.ACTIVE_GAME;
        for(int i = 0; i < board.rows(); i++){
            for(int j = 0; j < board.cols(); j++){
                board.set(new CellPosition(i, j), '-');
            }
        }
    }
}
