package no.uib.inf101.sem2.view;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.model.GameBoard;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class TestCellPositionToPixelConverter {
    
    @Test
    public void sanityTest() {
      GridDimension gd = new GameBoard(7, 7);
      CellPositionToPixelConverter converter = new CellPositionToPixelConverter(
          new Rectangle2D.Double(20, 70, 390, 390), gd, 5);

      Ellipse2D expected = new Ellipse2D.Double(135, 130, 50, 50);
      System.out.println(converter.getBoundsForCell(new CellPosition(1, 2)).getWidth());
      System.out.println(converter.getBoundsForCell(new CellPosition(1, 2)).getHeight());
      System.out.println(converter.getBoundsForCell(new CellPosition(1, 2)).getX());
      System.out.println(converter.getBoundsForCell(new CellPosition(1, 2)).getY());
      assertEquals(expected, converter.getBoundsForCell(new CellPosition(1, 2)));
    }
    
}
