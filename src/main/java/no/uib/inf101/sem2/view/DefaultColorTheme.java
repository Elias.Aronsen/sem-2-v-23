package no.uib.inf101.sem2.view;

import java.awt.Color;

/** Contains all the different colors used and allows for charachter to be used as subtitutes. */
public class DefaultColorTheme implements ColorTheme{

    @Override
    public Color getCellColor(Character c) {
        //Uppercase letters are from Tetrominos and lowercase letters are custom blocks.
        Color color = switch(c) {
            case 'r' -> Color.RED;
            case 'g' -> Color.GREEN;
            case 'y' -> Color.YELLOW;
            case 'c' -> Color.CYAN;
            case 'p' -> Color.PINK;
            case 'o' -> Color.ORANGE;
            case 'b' -> Color.BLUE;
            case 'm' -> Color.MAGENTA;
            case '-' -> getBackgroundColor();
            
            default -> throw new IllegalArgumentException(
                "No available color for '" + c + "'");
          };
        return color;
    }

    @Override
    public Color getFrameColor() {
        return new Color(0, 0, 0, 0);
    }
    @Override
    public Color getSecondaryFrameColor() {
        return Color.YELLOW;
    }

    @Override
    public Color getBackgroundColor() {
        return new Color(255, 255, 204);
    }

    @Override
    public Color getGameOverColor() {
        return new Color(255, 255, 255, 100);
    }

    @Override
    public Color getSelectedColor(ViewableGameModel model) {
        if(model.getColorCharacter() == 'r'){
            return Color.PINK;
        } else{
            return Color.CYAN;
        }
    }
    
}
