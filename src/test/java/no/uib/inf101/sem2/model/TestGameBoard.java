package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;

public class TestGameBoard {

    @Test
    public void testPrettyString() {
      GameBoard board = new GameBoard(3, 4);
      board.set(new CellPosition(0, 0), 'g');
      board.set(new CellPosition(0, 3), 'y');
      board.set(new CellPosition(2, 0), 'r');
      board.set(new CellPosition(2, 3), 'b');
      String expected = String.join("\n", new String[] {
          "g--y",
          "----",
          "r--b"
      });
      assertEquals(expected, board.prettyString());
    }
    

    @Test
    public void TestConnect4(){
        GameBoard board = getGameBoardWithContents(new String[] {
            "-----",
            "-r---",
            "-r---",
            "-r---",
            "-r---"
          });
          assertTrue(board.checkConnect4());//tests for connect4 on column

          board = getGameBoardWithContents(new String[] {
            "-----",
            "-r---",
            "-r---",
            "-r---",
            "bbbb-"
          });
          assertTrue(board.checkConnect4());//tests for connect4 on row

          board = getGameBoardWithContents(new String[] {
            "-----",
            "-r-b-",
            "-rbr-",
            "-brb-",
            "bbbr-"
          });
          assertTrue(board.checkConnect4()); //tests for connect4 on diagonal

          board = getGameBoardWithContents(new String[] {
            "-----",
            "-r---",
            "-r---",
            "-r---",
            "bbb--"
          });
          assertFalse(board.checkConnect4()); //checks if it returns false when no connect4

          board = getGameBoardWithContents(new String[] {
            "-bbbb",
            "-r---",
            "-r---",
            "-r---",
            "bbb--"
          });
          assertFalse(board.checkConnect4()); //board is not supposed to get connect 4 in the upper "invisible row".

          board = getGameBoardWithContents(new String[] {
            "-brbb",
            "-rb--",
            "-r-b-",
            "-r--b",
            "bbb--"
          });
          assertFalse(board.checkConnect4()); // same as above

    }

    public void TestFullBoard(){

        GameBoard board = getGameBoardWithContents(new String[] {
            "rbbrb",
            "brrbr",
            "rbbrb",
            "brrbr",
            "rbbrb"
          });
          assertFalse(board.checkConnect4());
          assertTrue(board.checkFullboard()); //checks fullboard

          board = getGameBoardWithContents(new String[] {
            "rbb-b",
            "brrbr",
            "rbbrb",
            "b-rbr",
            "rbbr-"
          });
          assertFalse(board.checkFullboard());
    }




    //////////////////////////////
    ////////HELPER METHODS////////
    //////////////////////////////

    /**
     * Takes in a string-representation of a  board, and returns a 
     * GameBoard with the corrrect dimensions and values.
     * @param str the string representation of the board.
     * @return a GameBoard object of the right parameters.
     */
    public static GameBoard getGameBoardWithContents(String[] str){
        int height = str.length;
        int length = str[0].length();
        GameBoard newBoard = new GameBoard(height, length);
  
        for(int i = 0; i < height; i++){    //iterates over the rows of string
            for(int j = 0; j < length; j++){ // iterates over a string
                newBoard.set(new CellPosition(i, j), str[i].charAt(j)); //Sets value in board according to string
            }
        }
        return newBoard;
  
    }
}
