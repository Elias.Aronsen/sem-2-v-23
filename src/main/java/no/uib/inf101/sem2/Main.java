package no.uib.inf101.sem2;

import no.uib.inf101.sem2.controller.GameController;
import no.uib.inf101.sem2.model.GameBoard;
import no.uib.inf101.sem2.model.GameModel;
import no.uib.inf101.sem2.view.GameView;

import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {

    // Makes the grid/board. Defined as 7x7 but will play as a 6x7 due to a invisible first row.

    GameBoard board = new GameBoard(7, 7);
    GameModel model = new GameModel(board);

    GameView view = new GameView(model);
    new GameController(model, view);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Connect 4 - Sem2 Elias Aronsen");
    frame.setContentPane(view);
    frame.setResizable(false);
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
  }
}
