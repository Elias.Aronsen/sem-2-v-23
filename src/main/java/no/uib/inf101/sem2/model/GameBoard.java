package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;

/** The actual game board, contains the dimensions of the board and the placement of all pieces on the board. */
public class GameBoard extends Grid<Character>{


    public GameBoard(int rows, int cols) {
        super(rows, cols,'-'); //uses the alternate super-constructor to make the default value '-'.
    }


    /**
     * Checks wheter if the board is completely filled.
     * @return Returns true if board is filled, false if not.
     */
    public boolean checkFullboard(){
        for(int i = 1; i < rows(); i++){
            for(int j = 0; j < cols(); j++){
                if(get(new CellPosition(i, j)).equals('-')){
                    return false;
                }
            }
        }
        return true;
    }



    /**
     * Returns a boolean of whether there is 4 pieces og the same color 
     * "in a row" including vertical and slanted connections.
     * @return returns true if there is a connection of 4
     */
    public boolean checkConnect4(){

        //check rows
        for(int i = 1; i < rows(); i++){
            for(int j = 3; j < cols(); j++){
                char val1 = get(new CellPosition(i, j-3));
                char val2 = get(new CellPosition(i, j-2));
                char val3 = get(new CellPosition(i, j-1));
                char val4 = get(new CellPosition(i, j));

                if((val1 == val2) && (val2 == val3) && (val3 == val4) && (val1 != '-')){
                    set(new CellPosition(i, j-3), 'm');
                    set(new CellPosition(i, j-2), 'm');
                    set(new CellPosition(i, j-1), 'm');
                    set(new CellPosition(i, j), 'm');
                    return true;
                }
            }
        }
        //check columns
        for(int j = 0; j < cols(); j++){
            for(int i = 4; i < rows(); i++){
                char val1 = get(new CellPosition(i-3, j));
                char val2 = get(new CellPosition(i-2, j));
                char val3 = get(new CellPosition(i-1, j));
                char val4 = get(new CellPosition(i, j));

                if((val1 == val2) && (val2 == val3) && (val3 == val4) && (val1 != '-')){
                    set(new CellPosition(i-3, j), 'm');
                    set(new CellPosition(i-2, j), 'm');
                    set(new CellPosition(i-1, j), 'm');
                    set(new CellPosition(i, j), 'm');
                    return true;
                }
            }
        }
        //check right leaning diagonals
        for(int i = 4; i < rows(); i++){
            for(int j = 0; j < cols()-3; j++){
                char val1 = get(new CellPosition(i, j));
                char val2 = get(new CellPosition(i-1, j+1));
                char val3 = get(new CellPosition(i-2, j+2));
                char val4 = get(new CellPosition(i-3, j+3));

                if((val1 == val2) && (val2 == val3) && (val3 == val4) && (val1 != '-')){
                    set(new CellPosition(i-3, j+3), 'm');
                    set(new CellPosition(i-2, j+2), 'm');
                    set(new CellPosition(i-1, j+1), 'm');
                    set(new CellPosition(i, j), 'm');
                    return true;
                }
            }
        }

        //check left leaning diagonals
        for(int i = 4; i < rows(); i++){
            for(int j = 3; j < cols(); j++){
                char val1 = get(new CellPosition(i, j));
                char val2 = get(new CellPosition(i-1, j-1));
                char val3 = get(new CellPosition(i-2, j-2));
                char val4 = get(new CellPosition(i-3, j-3));

                if((val1 == val2) && (val2 == val3) && (val3 == val4) && (val1 != '-')){
                    set(new CellPosition(i-3, j-3), 'm');
                    set(new CellPosition(i-2, j-2), 'm');
                    set(new CellPosition(i-1, j-1), 'm');
                    set(new CellPosition(i, j), 'm');
                    return true;
                }
            }
        }
        return false;
    }   


    
    /**
     * Makes a pretty String representation of our grid.
     * @return  a string composed of the character in that position.
     */
    public String prettyString(){
        String prettyString = "";
        for(int i = 0; i < this.rows(); i++){
            for(int j = 0; j < this.cols(); j++){
                prettyString += this.get(new CellPosition(i, j)); //Adds current character to string on current row
            }
            if(i < this.rows()-1)
                prettyString += "\n"; //Makes a new line every new row, but stops at
        }                             // the last for-loop to avoid empty bottom-line
        return prettyString;
    }

}
