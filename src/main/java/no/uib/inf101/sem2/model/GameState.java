package no.uib.inf101.sem2.model;


/** Contains the gamestates */
public enum GameState {
    WELCOME_SCREEN,
    ACTIVE_GAME,
    GAME_OVER;
}
