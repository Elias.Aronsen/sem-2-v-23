package no.uib.inf101.sem2.controller;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.model.GameState;

/** Interface that also defines the roles a model is needed for in the GameController class */
public interface ControllableGameModel {
    
    /**
     * Moves the current piece to a column deltaCol from original placement
     * @param deltaRow the amount of rows the piece moves.
     * @param deltaCol the amount of columns the piece moves 
     * to the left (negative values) or right (positive values)
     * @return Returns a boolean of wheter the movement was succesful.
     */
    boolean movePiece(int deltaRow, int deltaCol);

    /**
     * Drops the piece in the chosen column.
     */
    void dropPiece();


    /**
     * Gets current gamestate
     * @return returns Gamestate object
     */
    GameState getGameState();


    /**
   * Set the selected position in the grid.
   * 
   * @param selectedPosition new position to be selected, or null if new selection
   *                         should be the empty selection
   */
    void setSelected(CellPosition selectedPosition);

    /**
     * Move the selected column.
     * @param deltaCol amount and direction of movement.
     */
    boolean moveSelected(int deltaCol);

    /** Resets the board but keeps score. */
    void reset();

    /** Starts the game and leaves welcome screen */
    void startGame();
}
