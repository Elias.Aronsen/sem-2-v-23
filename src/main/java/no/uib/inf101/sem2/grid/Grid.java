package no.uib.inf101.sem2.grid;

import java.util.ArrayList;
import java.util.Iterator;

public class Grid<E> implements IGrid<E>{

    private int rows;
    private int cols;
    private ArrayList<GridCell<E>> grid;

    public Grid(int rows, int cols){

        if(rows<=0) {
			throw new IllegalArgumentException("Illegal amount of rows: " + rows);
		}
		if(cols<=0) {
			throw new IllegalArgumentException("Illegal amount of columns: " + cols);
		}

        this.rows = rows;
        this.cols = cols;

        grid = new ArrayList<GridCell<E>>();

        for(int i = 0; i < this.rows(); i++){
            for(int j = 0; j < this.cols(); j++){
                grid.add(new GridCell<E>(new CellPosition(i, j), null));
            }
        }
    }


    public Grid(int rows, int cols, E defaultValue){

        if(rows<=0) {
			throw new IllegalArgumentException("Illegal amount of rows: " + rows);
		}
		if(cols<=0) {
			throw new IllegalArgumentException("Illegal amount of columns: " + cols);
		}

        this.rows = rows;
        this.cols = cols;

        grid = new ArrayList<GridCell<E>>();

        for(int i = 0; i < this.rows(); i++){
            for(int j = 0; j < this.cols(); j++){
                grid.add(new GridCell<E>(new CellPosition(i, j), defaultValue));
            }
        }
    }



    @Override
    public int rows() {
        return this.rows;
    }

    @Override
    public int cols() {
        return this.cols;
    }

    @Override
    public Iterator<GridCell<E>> iterator() {
        ArrayList<GridCell<E>> it = new ArrayList<GridCell<E>>();
        for(int i = 0; i < this.rows(); i++){
            for(int j = 0; j < this.cols(); j++){
                it.add(new GridCell<E>(new CellPosition(i, j), grid.get(this.indexOf(i, j)).value()));
            }
        }
        return it.iterator();

    }

    @Override
    public void set(CellPosition pos, E value) {
        if(pos.row() >= rows() || pos.row() < 0) {
			throw new IndexOutOfBoundsException("Illegal amount of rows");
		}
		if(pos.col() >= cols() || pos.col() < 0) {
			throw new IndexOutOfBoundsException("Illegal amount of columns");
		}

        this.grid.set(indexOf(pos.row(), pos.col()), new GridCell<E>(pos, value));
    }

    @Override
    public E get(CellPosition pos) {
        if(pos.row() >= rows() || pos.row() < 0) {
			throw new IndexOutOfBoundsException("Illegal amount of rows");
		}
		if(pos.col() >= cols() || pos.col() < 0) {
			throw new IndexOutOfBoundsException("Illegal amount of columns");
		}

        return this.grid.get(indexOf(pos.row(), pos.col())).value();
    }

    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        return (pos.row() >= 0) && (pos.row() < this.rows()) && (pos.col() >= 0) && (pos.col() < this.cols());
    }


    /**
     * Takes in the given <code>row</code> and <code>col</code>
     * and spits out its position in our grid in the term of its index-position.
     * @param row the row
     * @param col the column
     * @return the equivalent position in the grid in terms of index.
     * @throws IndexOutOfBoundsException if the given values are out of bounds.
     */
    public Integer indexOf(int row, int col){ // finds the index of the cell in the list accordind to which column and row it is on
        if(row < 0 || row >= rows())
                throw new IndexOutOfBoundsException("Illegal row value");
            if(col < 0 || col >= cols())
                throw new IndexOutOfBoundsException("Illegal column value");
        return row*cols() + col;
      }
}
