package no.uib.inf101.sem2.controller;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.midi.GameSong;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.view.CellPositionToPixelConverter;
import no.uib.inf101.sem2.view.GameView;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

/** This class listens to the users key presses and mouse clicks and allows us to manipulate what we see */
public class GameController extends MouseAdapter implements KeyListener{

    private ControllableGameModel model;
    private GameView view;
    private GameSong song;

    public GameController(ControllableGameModel model, GameView view){
        this.model = model;
        this.view = view;
        this.view.addKeyListener(this);
        this.view.addMouseListener(this);
        this.view.setFocusable(true);
        this.song = new GameSong();

        this.song.run();
    }


    //allows for you to press which column you want to select.
    @Override
    public void mousePressed(MouseEvent event) {
        if(model.getGameState() != GameState.GAME_OVER){
            Point2D mouseCoordinate = event.getPoint();
            CellPositionToPixelConverter converter = this.view.getCellPositionToPixelConverter();
            CellPosition pos = converter.getCellPositionOfPoint(mouseCoordinate);
            if(pos == null){ //this catches cases where user clicks out of bounds
                return;
            }
            this.model.setSelected(pos);
            this.view.repaint();
        }
    }
  
    @Override
    public void keyReleased(KeyEvent e) {
        //universal keys that are allowed to be used nomatter the gamestate
        if (e.getKeyCode() == KeyEvent.VK_Q) {
            // Q-key was pressed
            System.exit(0);
        }else if (e.getKeyCode() == KeyEvent.VK_Y) {
            // Y was pressed
            this.song.doUnpauseMidiSounds();
        }else if (e.getKeyCode() == KeyEvent.VK_N) {
            // N was pressed
            this.song.doPauseMidiSounds();
        }

        //keys allowed for welcome screen
        if(model.getGameState() == GameState.WELCOME_SCREEN){
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                // Enter was pressed
                this.model.startGame();
                
            }else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                // Spacebar was pressed
                this.model.startGame();
            }
        }
        else if(model.getGameState() == GameState.ACTIVE_GAME){ //key presses for when you are playing the game
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                // Left arrow was pressed
                this.model.movePiece(0,-1);
                this.model.moveSelected(-1);
            }
            else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                // Right arrow was pressed
                this.model.movePiece(0,1);
                this.model.moveSelected(1);
            }
            else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                // Spacebar was pressed
                this.model.dropPiece();
            }
            else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                // Spacebar was pressed
                this.model.dropPiece();
            }
        } else if (model.getGameState() == GameState.GAME_OVER){ // round over key presses
            if (e.getKeyCode() == KeyEvent.VK_R) {
                // R-key was pressed
                model.reset();
            }
        }
        this.view.repaint(); //repaints the view every time you manipulate it
    }


    @Override
    public void keyTyped(KeyEvent e) {
        
    }
    @Override
    public void keyPressed(KeyEvent e) {
    }
}
