package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.grid.CellPosition;
import java.util.Objects;

/** The class for our playing pieces. */
public class Piece {
    private final Character color;
    private final CellPosition pos;

    public Piece(Character color, CellPosition pos){
        this.color = color;
        this.pos = pos;
    }

    static Piece newPiece(Character colorOfPiece){
        CellPosition defaultCellPosition = new CellPosition(0, 0);
        return new Piece(colorOfPiece, defaultCellPosition);
    }

    public CellPosition getPos(){
        return this.pos;
    }


    /**
     * Shifts a copy of the piece by the given column value.
     * @param deltaRow How much the the piece changes in rows.
     * @param deltaCol How much the the piece changes in columns.
     * @return Returns shifted copy of piece.
     */
    public Piece shiftedBy(int deltaRow, int deltaCol){
        CellPosition newPos = new CellPosition(pos.row() + deltaRow, pos.col() + deltaCol);
        return new Piece(this.color, newPos);
    }



    @Override
    public boolean equals(Object obj) {
        Piece piece = (Piece) obj;

        if(this.color != piece.color){ // Checks if they have the same character
            return false;
        }
        if(this.pos.row() != piece.pos.row() || this.pos.col() != piece.pos.col()){ //Checks if they have the same position.
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, pos);
    }

}
