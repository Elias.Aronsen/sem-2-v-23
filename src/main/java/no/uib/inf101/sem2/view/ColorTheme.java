package no.uib.inf101.sem2.view;

import java.awt.Color;


public interface ColorTheme {
    
    /**
     *  Takes in a character and finds the corresponding color.
     * @param c is a character corresponding to a certain color
     * @return the color that corresponds to the character <code>c</code>
     */
    Color getCellColor(Character c);


    /**
     * Gets the designated color for the frame
     * @return color for the frame
     */
    Color getFrameColor();

    /** Gets the designated color for the visible frame*/
    Color getSecondaryFrameColor();
    
    /**
     * Gets the designated color for the background
     * @return color for the background 
     */
    Color getBackgroundColor();

    /**
     * Gets the designated color for the game over screen
     * @return color for the game over screen
     */
    Color getGameOverColor();

    /**Gets the designated color for the currently selected column */
    Color getSelectedColor(ViewableGameModel model);
}
