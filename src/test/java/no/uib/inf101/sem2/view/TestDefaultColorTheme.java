package no.uib.inf101.sem2.view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import java.awt.Color;

public class TestDefaultColorTheme {
    

    @Test
    public void sanityTestDefaultColorTheme() {
      ColorTheme colors = new DefaultColorTheme();
      assertEquals(new Color(255, 255, 204), colors.getBackgroundColor());
      assertEquals(new Color(0, 0, 0, 0), colors.getFrameColor());
      assertEquals(new Color(255, 255, 204), colors.getCellColor('-'));
      assertEquals(Color.RED, colors.getCellColor('r'));
      assertEquals(Color.BLUE, colors.getCellColor('b'));
      assertThrows(IllegalArgumentException.class, () -> colors.getCellColor('\n'));
    }
    

}
