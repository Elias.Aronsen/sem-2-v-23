package no.uib.inf101.sem2.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Font;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.Piece;

/** Where we draw all of our graphics */
public class GameView extends JPanel{
 
    ViewableGameModel model;
    ColorTheme colorTheme;
    double INNER_MARGIN = 5; // margin between cells in grid
    double OUTER_MARGIN = 20; //margin between box and sides of window
    double EXTRA_MARGIN = 50; //margin between box and top of window 
    int s = 50; // preffered size of cell in grid


    public GameView(ViewableGameModel model) {
        this.setFocusable(true);
    
        this.model = model;
    
        this.setPreferredSize(this.getPreferredSize());
    
        this.colorTheme = new DefaultColorTheme();
        this.setBackground(colorTheme.getBackgroundColor());
        
    }

    
    @Override
    public void paintComponent(Graphics g) {

      super.paintComponent(g);
      Graphics2D g2 = (Graphics2D) g;

      if(model.getGameState() == GameState.WELCOME_SCREEN){
        drawWelcomeScreen(g2);
      }else if(model.getGameState() == GameState.ACTIVE_GAME){
        drawGame(g2);
      } else if(model.getGameState() == GameState.GAME_OVER){
        drawGameOver(g2);
      }
    }



    /** Draws the welcome screen. */
    private void drawWelcomeScreen(Graphics2D g2){

      //skjerm
      Rectangle2D box = new Rectangle2D.Double(0, 0, getPreferredSize().width, getPreferredSize().height);
      g2.setColor(colorTheme.getGameOverColor());
      g2.fill(box);
      //tekst
      String titleText = "CONNECT 4!";

      g2.setFont(new Font("Tahoma", Font.BOLD, 50));
      g2.setColor(colorTheme.getCellColor('m'));
      Inf101Graphics.drawCenteredString(g2, titleText, getPreferredSize().width/2,getPreferredSize().height/5);

      String startText = "Press 'Enter' or 'Space' to start!";
      String musicText = "Want music? 'y'/'n'";

      //draw start-game text
      g2.setFont(new Font("Tahoma", Font.ROMAN_BASELINE, 25));
      g2.setColor(Color.DARK_GRAY);
      Inf101Graphics.drawCenteredString(g2, startText, getPreferredSize().width/2,3*getPreferredSize().height/5);

      //draw music text
      g2.setFont(new Font("Tahoma", Font.ITALIC, 15));
      g2.setColor(Color.GRAY);
      Inf101Graphics.drawCenteredString(g2, musicText, getPreferredSize().width/2,4*getPreferredSize().height/5);

      drawQuitText(g2);
    } 





  /**
   *  Draws the entire Connect 4 game.
   * @param g2 Graphics2D-object which is the canvas were working on
   */
  private void drawGame(Graphics2D g2) {

    //Draws the invisible frame used for the grid and cells.
    Rectangle2D box = drawBox(this.model.getDimension(),0);
    g2.setColor(this.colorTheme.getFrameColor());
    g2.fill(box); 
    
    int rows = this.model.getDimension().rows();
    int cols = this.model.getDimension().cols();
    Rectangle2D box2 = drawBox(new Grid<>(rows-1, cols), 53); //draws the visble yellow board that we will be playing in.
    g2.setColor(colorTheme.getSecondaryFrameColor());
    g2.fill(box2);
    g2.setColor(Color.BLACK);
    g2.draw(box2); // draws outline for the box

    drawCurrentTurn(g2);
    drawScore(g2);

    CellPositionToPixelConverter converter = getCellPositionToPixelConverter();

    drawCells( g2, this.model.getTilesOnBoard(), converter, this.colorTheme);
    drawPiece(g2, this.model.getPiece(), converter, colorTheme);
    drawQuitText(g2);

    //draws small additional music pause-unpause instructions
    g2.setFont(new Font("Tahoma", Font.ITALIC, 15));
    g2.setColor(Color.GRAY);
    Inf101Graphics.drawCenteredString(g2, "Music? y/n", 7*getPreferredSize().width/8, getPreferredSize().height -12);

  }



  /**
   *  Draws our cells/tiles into the right coordinates in the right color and size according to our parameters.
   * @param g2 Our canvas, which we paint onto.
   * @param grid Our Iterable grid, which contains our tiles
   * @param converter Our converter, to allow us to convert our rows and columns into pixels
   * @param ct Our ColorTheme which dictates the color of the board, tiles and pieces
   */
  private void drawCells(Graphics2D g2, Iterable<GridCell<Character>> grid, CellPositionToPixelConverter converter, ColorTheme ct){
    
    for(GridCell<Character> cell : grid){
      Ellipse2D circle = converter.getBoundsForCell(cell.pos()); //change getboundforcell for new method
      Color color = ct.getCellColor(cell.value());
      
      int currentCol = cell.pos().col(); 
      int selectedCol = this.model.getSelected().col();
      if((currentCol == selectedCol) && cell.value() == '-'){
        color = ct.getSelectedColor(model);
      }
      if(cell.pos().row() == 0){
        color = new Color(0, 0, 0, 0);
      }

      g2.setColor(color);
      g2.fill(circle);
      g2.setColor(Color.BLACK);
      g2.draw(circle);
    }
  }



  /** Draws the current playing piece. */
  private void drawPiece(Graphics2D g2, Piece piece, CellPositionToPixelConverter converter, ColorTheme ct){

    char currentColor = this.model.getColorCharacter();

    Ellipse2D circle = converter.getBoundsForCell(piece.getPos());
    Color color = ct.getCellColor(currentColor);

    g2.setColor(color);
    g2.fill(circle);
  }



  /** Draws small text showing how to quit game */
  private void drawQuitText(Graphics2D g2){

    //quit text
    String quitText = "Press 'Q' to close window.";
    g2.setColor(Color.BLACK);
    g2.setFont(new Font("Tahoma", Font.BOLD, 15));
    Inf101Graphics.drawCenteredString(g2, quitText, getPreferredSize().width/2, getPreferredSize().height-12);
  }



/** Draws a rectangle with the given dimensions of the board with a margin */
  private Rectangle2D drawBox(GridDimension gd, int Y_MARGIN){
    /**
     * BoxDimensionX og BoxDimensionY er lignende getPrefferedSize bare uten OUTER_MARGIN og gir oss
     * den perfekte boksen for at rutene skal ha 50 piksler i størrelse.
     * Altså disse variablene gjør sånn at man kan lett forandre størrelsen på:
     * rutene, INNER_MARGIN, OUTER_MARGIN, rows og cols,
     * uten at man får rare rutenett med stygt utseende.
     */
    double boxDimensionX = (s + INNER_MARGIN)*gd.cols() + INNER_MARGIN;
    double boxDimensionY = (s + INNER_MARGIN)*gd.rows() + INNER_MARGIN;

    return new Rectangle2D.Double( OUTER_MARGIN, OUTER_MARGIN + EXTRA_MARGIN + Y_MARGIN, boxDimensionX, boxDimensionY);
  }




  /** Simply gets the CPTPC with the boards dimensions */
  public CellPositionToPixelConverter getCellPositionToPixelConverter(){

    Rectangle2D box = drawBox(this.model.getDimension(), 0);
    return new CellPositionToPixelConverter(box, this.model.getDimension(), INNER_MARGIN);
  }




  /**
   * Dette er funksjon for som tar inn gitte verdier som <code>s</code> (for cell størrelse), <code>margin</code>,
   *  <code>col</code>, og <code>rows</code> og regner ut hva dimensjonene til viduet burde være.
   */
  @Override
  public Dimension getPreferredSize(){

    int width = (int) ((s + INNER_MARGIN) * model.getDimension().cols() + INNER_MARGIN + 2*OUTER_MARGIN);
    int height = (int) ((s + INNER_MARGIN) * model.getDimension().rows() + INNER_MARGIN + 2*OUTER_MARGIN + EXTRA_MARGIN);
    return new Dimension(width, height);
  }



  /** Draws the text displaying the current turn */
  private void drawCurrentTurn(Graphics2D g2){

    g2.setColor(colorTheme.getSelectedColor(model));
    g2.setFont(new Font("Tahoma", Font.BOLD, 40));
    String currentCol = "Blue's";
    if(model.getColorCharacter() == 'r'){
      currentCol = "Red's";
    }
    int dimX = getPreferredSize().width/10;
    g2.drawString(currentCol + " turn.", dimX, 40);
  }



  /** Draws the current amount of wins between blue and red. */
  private void drawScore(Graphics2D g2){

    String scoreTextBlue ="Blue: " + model.getBlueWins();
    String scoreTextRed ="Red: " + model.getRedWins();

    //red
    g2.setColor(colorTheme.getCellColor('r'));
    g2.setFont(new Font("Tahoma", Font.BOLD, 15));

    int dimX = 4*getPreferredSize().width/5;
    g2.drawString(scoreTextRed, dimX, 20);

    //blue
    g2.setColor(colorTheme.getCellColor('b'));
    g2.setFont(new Font("Tahoma", Font.BOLD, 15));
    g2.drawString(scoreTextBlue, dimX, 40);
    
  }



  /** Draws the game over screen */
  private void drawGameOver(Graphics2D g2){

    Rectangle2D box = new Rectangle2D.Double(0, 0, getPreferredSize().width, getPreferredSize().height);
    g2.setColor(colorTheme.getGameOverColor());
    g2.fill(box);

    //Specifies game over text to winner
    String winner = "Huh?";
    if(model.getWinner() == 'b'){
      winner = "Blue";
      g2.setColor(colorTheme.getCellColor('b'));

    } else if(model.getWinner() == 'r'){
      winner = "Red";
      g2.setColor(colorTheme.getCellColor('r'));

    } else if(model.getWinner() == '-'){
      winner = "No one";
      g2.setColor(colorTheme.getCellColor('m'));
    }

    String gameOverText = winner + " won! Play again? Press 'R'.";

    g2.setFont(new Font("Tahoma", Font.BOLD, 20));
    Inf101Graphics.drawCenteredString(g2, gameOverText, box);
    drawQuitText(g2);
  }


}
