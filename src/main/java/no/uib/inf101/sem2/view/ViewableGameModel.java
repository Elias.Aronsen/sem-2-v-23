package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.model.GameState;
import no.uib.inf101.sem2.model.Piece;

/** Interface that also defines the roles a model is needed for in the GameView class */
public interface ViewableGameModel {

    /**
     * @return An Object that implements GridDimension
     */
    GridDimension getDimension();


    /**
     * @return An Iterable Object that implements 
     * Iterable<''GridCell'<'Character'>'>
     */
    Iterable<GridCell<Character>> getTilesOnBoard();

    /**
     * @return An Iterable Object that implements 
     * Iterable<''GridCell'<'Character'>'>
     */
//    Iterable<GridCell<Character>> getPieceOnBoard();

    /**
     * Gets current GameState.
     * @return  Current GameState: Either ACTIVE_GAME, GAME_OVER or WELCOME_SCREEN.
     */
    GameState getGameState();

    /**
     * @return Returns the character r or b according to whose turn it is.
     */
    Character getColorCharacter();

    /** Gets the selected cell in the grid.  */
    CellPosition getSelected();
    
    /**Returns the current piece */
    Piece getPiece();

    /** Gets the amount of wins for red */
    int getRedWins();
    
    /** Gets the amount of wins for blue */
    int getBlueWins();

    /** Gets the winner of the round
     * @return Returns: 'b' for blue, 'r' for red and null for a tie.
     */
    char getWinner();
}
