package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;

public class TestGameModel {

    @Test
    public void testDropPiece(){
        GameBoard board = new GameBoard(5, 5);
          GameModel model = new GameModel(board);

          //drop a piece to board
          model.dropPiece();
    
          String expected = String.join("\n", new String[] {
            "-----",
            "-----",
            "-----",
            "-----",
            "--r--"
        });
        assertEquals(expected, board.prettyString());

          //drop a few pieces to board
          model.dropPiece();
          model.dropPiece();
          model.dropPiece();
          model.dropPiece();
          model.dropPiece();
          model.dropPiece(); //these last two drops should not work because the first row shoudnt accept drops
    
          expected = String.join("\n", new String[] {
            "-----",
            "--b--",
            "--r--",
            "--b--",
            "--r--"
        });
        assertEquals(expected, board.prettyString());

        //these tests have also tested the fact that new pieces will always drop in the center if not moved
        // and have also tested the fact that the colors will bew switched between r and b every round

        board = new GameBoard(6, 6);
        model = new GameModel(board);

        model.dropPiece();
        model.dropPiece();
        model.dropPiece();
        model.dropPiece();
        model.dropPiece();
        model.dropPiece();

        expected = String.join("\n", new String[] {
            "------",
            "---r--",
            "---b--",
            "---r--",
            "---b--",
            "---r--"
        });
        assertEquals(expected, board.prettyString());
    }


    @Test
    public void testMovePiece(){

        GameBoard board = new GameBoard(5, 5);
        GameModel model = new GameModel(board);

        model.movePiece(0, 1);
        model.dropPiece();

        //checks if movePiece moves piece one to the right.
        String expected = String.join("\n", new String[] {
            "-----",
            "-----",
            "-----",
            "-----",
            "---r-"
        });
        assertEquals(expected, board.prettyString());

        expected = String.join("\n", new String[] {
            "-----",
            "-----",
            "-----",
            "-----",
            "---r-"
        });
        assertEquals(expected, board.prettyString());

        //checks if it moves it to the left
        model.movePiece(0, -2);
        model.dropPiece();

        expected = String.join("\n", new String[] {
            "-----",
            "-----",
            "-----",
            "-----",
            "b--r-"
        });
        assertEquals(expected, board.prettyString());

        //checks if it returns false if move was not completed, legalmove should prohibit illegal moves
        assertFalse(model.movePiece(0, 100));



        board = TestGameBoard.getGameBoardWithContents(new String[] {
            "-r-r-",
            "-bbb--",
            "-----",
            "-----",
            "-----"
          });
          model = new GameModel(board);
        assertFalse(model.movePiece(0, 1));
        assertFalse(model.movePiece(0, -1));
        assertFalse(model.movePiece(1, 0));
        assertFalse(model.movePiece(-1, 0));
        assertFalse(model.movePiece(1, 1));
        assertFalse(model.movePiece(1, -1));

        assertTrue(model.movePiece(2, 0));

    }

    @Test
    public void testSelection(){
        GameBoard board = new GameBoard(5, 5);
        GameModel model = new GameModel(board);

        model.setSelected(new CellPosition(0, 0));
        model.dropPiece();

        //checks if selection impacts drop location
        String expected = String.join("\n", new String[] {
            "-----",
            "-----",
            "-----",
            "-----",
            "r----"
        });
        assertEquals(expected, board.prettyString());

        model.setSelected(new CellPosition(0, 0));
        model.dropPiece();
        model.dropPiece();

        //checks selection doesnt impact start position and that new pieces and 
        // selected position always start in middle on a new round
        expected = String.join("\n", new String[] {
            "-----",
            "-----",
            "-----",
            "b----",
            "r-r--"
        });
        assertEquals(expected, board.prettyString());



        //checks if selected position stops at border of board.

        assertTrue(model.moveSelected(1));
        assertTrue(model.moveSelected(1));
        assertFalse(model.moveSelected(1));
        model.dropPiece();

        expected = String.join("\n", new String[] {
            "-----",
            "-----",
            "-----",
            "b----",
            "r-r-b"
        });
        assertEquals(expected, board.prettyString());

    }
}
